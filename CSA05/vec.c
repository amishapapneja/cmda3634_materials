#include "vec.h"

/* calculates ||u-v||^2 */
double vec_dist_sq (double* u, double* v, int dim) {
    double dist_sq = 0;
    for (int i=0;i<dim;i++) {
        dist_sq += (u[i]-v[i])*(u[i]-v[i]);
    }
    return dist_sq;
}

/* w = u + v */
void vec_add (double* u, double* v, double* w, int dim) {
    for (int i=0;i<dim;i++) {
        w[i] = u[i] + v[i];
    }
}

/* w = cv */
void vec_scalar_mult (double* v, double c, double* w, int dim) {
    for (int i=0;i<dim;i++) {
        w[i] = v[i]*c;
    }
}

/* performs the deep copy v->data[i] = w->data[i] for all i */
void vec_copy (double* v, double* w, int dim) {
    for (int i=0;i<dim;i++) {
        v[i] = w[i];
    }
}

/* zeros the vector v */
void vec_zero (double* v, int dim) {
    for (int i=0;i<dim;i++) {
        v[i] = 0;
    }
}
