#ifndef ARRAY_H
#define ARRAY_H

typedef struct array_s {
    double* data;
    int size;
} array_type;

void array_zeros(array_type* array, int size);

void array_set(array_type* array, int index, double element);

double array_get(array_type* array, int index);

void array_free(array_type* array);

#endif
