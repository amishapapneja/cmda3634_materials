#include <stdio.h>
#include <stdlib.h>
#include "array.h"

void array_zeros(array_type* array, int size) {
    array->data = (double*)calloc(size,sizeof(double));
    if (array->data == 0) {
        printf ("calloc failed in array_zeros!\n");
        exit(1);
    }
    array->size = size;
}

void array_set(array_type* array, int index, double element) {
    if ((index < 0) || (index >= array->size)) {
        printf ("array index %d out of bounds in array_set!\n",index);
        exit(1);
    }
    array->data[index] = element;
}

double array_get(array_type* array, int index) {
    if ((index < 0) || (index >= array->size)) {
        printf ("array index %d out of bounds in array_get!\n",index);
        exit(1);
    }
    return array->data[index];
}

void array_free(array_type* array) {
    if (array->data == 0) {
        printf ("null data pointer in array_free!\n");
        exit(1);
    }
    free(array->data);
    array->data = 0;
}
