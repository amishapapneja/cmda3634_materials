#include <stdio.h>

int main () {
    long int number;
    long int sum = 0;
    int k = 0;
    while (scanf ("%ld",&number) == 1) {
        k += 1;
        sum += number;
    }
    printf ("The sum of your %d numbers is %ld\n",k,sum);
}
