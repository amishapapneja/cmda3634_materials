#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pri_queue.h"

#define MAX_NUM_WORDS 20000
#define WORD_SIZE 5
#define SCANF_FORMAT "%6s"

char words[MAX_NUM_WORDS][WORD_SIZE+1];

int main (int argc, char** argv) {

    /* read k from the command line */
    if (argc < 2) {
        printf ("error : command usage %s %s\n",argv[0],"k");
        return 1;
    }
    int k = atoi(argv[1]);

    /* read the valid Wordle words from the file */
    int num_words = 0;
    char word[WORD_SIZE+2];
    while (scanf(SCANF_FORMAT,word) == 1) {
        if (strlen(word) != WORD_SIZE) {
            printf ("error : length of word %d in file is not WORD_SIZE!\n",num_words+1);
            return 1;
        }
        if (num_words >= MAX_NUM_WORDS) {
            printf ("error : too many words in file!\n");
            return 1;
        }
        /* should also check that each word has only lower case letters (exercise) */
        strcpy(words[num_words],word);
        num_words += 1;
    }

    /* count the number of times each letter occurs in each blank */
    int count[WORD_SIZE][26];
    for (int i = 0;i<WORD_SIZE;i++) {
        for (int j = 0;j<26;j++) {
            count[i][j] = 0;
        }
    }
    for (int i=0;i<num_words;i++) {
        for (int j=0;j<WORD_SIZE;j++) {
            count[j][words[i][j]-'a'] += 1;
        }
    }

    /* compute a score for each word and insert scores into priority queue */
    pri_queue pq;
    pri_queue_element element;
    pri_queue_init (&pq,num_words);
    for (int i=0;i<num_words;i++) {
        int score = 0;
        for (int j=0;j<WORD_SIZE;j++) {
            score += count[j][words[i][j]-'a'];
        }
        element.priority = score;
        element.data = i;
        pri_queue_insert (&pq,element);
    }

    /* use the priority queue to print out the top k starting words */
    for (int i=0;i<k;i++) {
        element = pri_queue_peek_top(&pq);
        printf ("word %s has score %.0lf\n",words[element.data],element.priority);
        pri_queue_delete_top(&pq);
    }

    /* free the priority queue */
    pri_queue_free(&pq);

}
