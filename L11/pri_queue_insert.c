#include <stdio.h>
#include <stdlib.h>
#include "pri_queue.h"

void pri_queue_init(pri_queue* pq, int max_size) {
    pq->elements = (pri_queue_element*)malloc(max_size*sizeof(pri_queue_element));
    if (pq->elements == 0) {
        printf ("error : malloc failed in pri_queue_init\n");
        exit(1);
    }
    pq->max_size = max_size;
    pq->size = 0;
}

int pri_queue_size(pri_queue* pq) {
    return pq->size;
}

pri_queue_element pri_queue_peek_top(pri_queue* pq) {
    if (pq->size == 0) {
        printf ("error : underflow in pri_queue_peek_top\n");
        exit(1);
    }
    return pq->elements[pq->size-1];
}

void pri_queue_insert(pri_queue* pq, pri_queue_element element) {
    if (pq->size >= pq->max_size) {
        printf ("error : overflow in pri_queue_insert\n");
        exit(1);
    }
    int place = 0;
    for (;place<pq->size;place++) {
        if (element.priority < pq->elements[place].priority) {
            break;
        }
    }
    for (int i=pq->size;i>place;i--) {
        pq->elements[i] = pq->elements[i-1];
    }
    pq->elements[place] = element;
    pq->size += 1;
}

void pri_queue_delete_top(pri_queue* pq) {
    if (pq->size == 0) {
        printf ("error : underflow in pri_queue_delete_top\n");
        exit(1);
    }
    pq->size -= 1;
}

void pri_queue_free(pri_queue* pq) {
    if (pq->elements == 0) {
        printf ("error : null elements pointer in pri_queue_free\n");
        exit(1);
    }
    free (pq->elements);
    pq->elements = 0;
}
