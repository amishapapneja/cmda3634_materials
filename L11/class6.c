#include <stdio.h>

int main () {
    int A[3][4] = { {1, 2, 3, 4 }, {5, 6, 7, 8}, {9, 10, 11, 12} };
    int B[3*4] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
    int* A_ptr = &(A[0][0]);
    int* B_ptr = &(B[0]);
#if 1
    for (int i=0;i<3*4;i++) {
        printf ("A_ptr[%d] = %d, B_ptr[%d] = %d\n",i,A_ptr[i],i,B_ptr[i]);
    }
#endif
#if 0
    for (int i=0;i<3;i++) {
        for (int j=0;j<4;j++) {
            printf ("A[%d][%d] = %d, B_ptr[%d*4+%d] = %d\n",i,j,A[i][j],i,j,B_ptr[i*4+j]);
        }
    } 
#endif
#if 0
    for (int i=0;i<3;i++) {
        for (int j=0;j<4;j++) {
            printf ("A[%d][%d] = %d, A_ptr[%d*4+%d] = %d\n",i,j,A[i][j],i,j,A_ptr[i*4+j]);
        }
    } 
#endif
}

