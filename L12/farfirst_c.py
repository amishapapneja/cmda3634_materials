import sys # for command line arguments
import numpy as np # for matrix processing
import ctypes as ct # for calling C from Python

# load C functions for CSA03
clib = ct.cdll.LoadLibrary("./farfirst.so")
clib.calc_cost_sq.restype = ct.c_double

# make sure command line arguments are provided 
if (len(sys.argv) < 3):
    print ('command usage :',sys.argv[0],'datafile','k')
    exit(1)

# read datafile name from the command line
datafile = sys.argv[1]

# read k from the command line
k = int(sys.argv[2])

# read the points
# float64 is equivalent to a C double
points = np.loadtxt(datafile,comments='#',dtype='float64')
print (points.flags)
points_cptr = points.ctypes.data_as(ct.POINTER(ct.c_double));
rows,cols = points.shape;

# find k centers using the farthest first algorithm
# int32 is equivalent to a C integer
centers = np.zeros(k,dtype='int32')
centers_cptr = centers.ctypes.data_as(ct.POINTER(ct.c_int));

# print the approximate minimal cost for the k-center problem
#cost = np.round(np.sqrt(cost_sq),4)
#print ("approximate optimal cost =",cost);

# print an approx optimal solution to the k-center problem
#print ("approx optimal centers: ",end="");
#print (centers)
