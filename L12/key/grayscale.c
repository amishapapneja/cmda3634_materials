#include <stdio.h>
#include <math.h>

typedef unsigned char byte;
void grayscale(byte* color_image, byte* gray_image, int rows, int cols) {
    byte* rgb = color_image;
    byte* gray = gray_image;
    for (int i=0;i<rows;i++) {
        for (int j=0;j<cols;j++) {
            *gray = round(0.299*rgb[0]+0.587*rgb[1]+0.114*rgb[2]);
            gray += 1;
            rgb += 3;
        }
    }
}
