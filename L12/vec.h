#ifndef VEC_H
#define VEC_H

/* calculates ||u-v||^2 */
double vec_dist_sq (double* u, double* v, int dim);

#endif 

