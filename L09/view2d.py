import sys
import numpy as np
import matplotlib.pyplot as plt

# read the name of the image file is a command line argument
if (len(sys.argv) < 2):
    print ("Command Usage : python3",sys.argv[0],"imagefile")
    exit(1)
imagefile = sys.argv[1]

# read the data file
data = np.loadtxt(sys.stdin,comments='#')

# plot the data
plt.gca().set_aspect('equal')
plt.scatter(data[:,0],data[:,1],s=10,color='black')

# plot the special points (if additional command line argments present)
for k in range(2,len(sys.argv)):
    i = int(sys.argv[k])
    plt.scatter (data[i,0],data[i,1],s=100,color='orange')

# save the plot as an image
plt.savefig(imagefile)

