#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_ARRAY_SIZE 1000

int main (int argc, char** argv) {

    /* array for storing the sorted numbers read from the file */
    double sorted[MAX_ARRAY_SIZE];

    /* read numbers and put them into sorted order */
    int n = 0;
    double number;
    while (scanf("%lf",&number) == 1) {
        if (n < MAX_ARRAY_SIZE) {
            int place = 0;
            while (number > sorted[place] && place < n) {
                place += 1;
            }
            for (int i=n;i>place;i--) {
                sorted[i] = sorted[i-1];
            }
            sorted[place] = number;
            n += 1;
        } else {
            printf ("Too many numbers!\n");
            return 1;
        }
    }

    /* print the sorted numbers */
    for (int i=0;i<n;i++) {
        printf ("%lf\n",sorted[i]);
    }
}
