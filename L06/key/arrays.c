#include <stdio.h>
#include <stdlib.h>

int main () {
    int u[5] = { 1, 2, 3, 4, 5 };
    int v[5] = { -1, 3, -2, 5, 9 };
    int w[5];
    int A[3][3] = { { 1, 2, 3}, {4, 5, 6}, { 7, 8, 9 } };
    int x[3] = { 1, 1, 1 };
    int b[3];

    printf ("The second element of u is %d\n",u[1]);
    printf ("Element A_23 is %d\n",A[1][2]);

    /* calculate w = u + v */
    for (int i=0;i<5;i++) {
        w[i] = u[i]+v[i];
    }

    /* print w */
    for (int i=0;i<5;i++) {
        printf ("w_%d = %d\n",i,w[i]);
    }

    /* calculate b=Ax */
    for (int i=0;i<3;i++) {
        b[i] = 0;
        for (int j=0;j<3;j++) {
            b[i] += A[i][j]*x[j];
        }
    }

    /* print b=Ax */
    for (int i=0;i<3;i++) {
        printf ("b_%d = %d\n",i,b[i]);
    }
}
