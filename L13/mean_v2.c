#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vec.h"

int main () {

    /* read the shape of the data matrix */
    int rows, cols;
    if (scanf("%*c %d %d",&rows, &cols) != 2) {
        printf ("error reading the shape of the data matrix\n");
        return 1;
    }

    /* dynamically allocate memory for the data matrix */
    /* note: this line is roughly equivalent to */
    /* double data[rows*cols] */
    /* but the data memory is located on the heap rather than the stack */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* read the data matrix */
    for (int i=0;i<rows;i++) {
        for (int j=0;j<cols;j++) {
            if (scanf("%lf",data+i*cols+j) != 1) {
                printf ("error reading data matrix\n");
                return 1;
            }
        }
    }

    /* calculate the mean */
    double mean[cols];
    for (int i=0;i<rows;i++) {
        vec_add (mean,data+i*cols,mean,cols);
    }
    vec_scalar_mult(mean,1/rows,mean,cols);

    /* print the result */
    for (int i=0;i<cols;i++) {
        printf ("%.5lf ",mean[i]);
    }
    printf ("\n");

    /* free the dynamically allocated memory */
    free (data);
}
