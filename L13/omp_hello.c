#include <stdio.h>
#include <stdlib.h>

int main (int argc, char** argv) {

    /* get thread_count from command line */
    if (argc < 2) {
        printf ("Command usage : %s %s\n",argv[0],"thread_count");
        return 1;
    }
    int thread_count = atoi(argv[1]);

    printf ("Hello World!\n");
}
