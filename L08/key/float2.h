#ifndef FLOAT2_H
#define FLOAT2_H

typedef struct float2_s {
    float x, y;
} float2;

float2 float2_add (float2 u, float2 v);

float2 float2_scalar_mult (float2 v, float c);

void float2_print (float2 v, char* name);

#endif 

